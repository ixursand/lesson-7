let min = document.getElementById("min");
let sec = document.getElementById("sec");
let cnt = (localStorage.getItem('count') != null) ? localStorage.getItem('count') : 0;
cnt = parseInt(cnt);
let timer=0;
document.addEventListener('DOMContentLoaded', function(){ 
				minsec(cnt);		
});
function counter(action){
	if(action == 1 && timer == 0){
		timer = setInterval(function(){
				cnt++;
				minsec(cnt);
		},1000);
		
	}
	if(action == 0){
		localStorage.setItem('count',(parseInt(min.innerHTML)*60)+parseInt(sec.innerHTML));
		clearInterval(timer);
		timer = 0;
	}
	if(action == 2){
		localStorage.setItem('count',0);
		cnt = 0;
		minsec(0);
		clearInterval(timer);
		timer = 0;
	}
   
}
function minsec(s){
		if(parseInt(s/60)<10) min.innerHTML = "0"+parseInt(s/60);
		else min.innerHTML = parseInt(s/60);
		if((s%60)<10) sec.innerHTML = "0"+s%60;
		else sec.innerHTML = s%60;	
}
